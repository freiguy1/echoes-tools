use super::{Planet, PlanetResource, System, SystemGraphBag};
use csv::ReaderBuilder;
use petgraph::Graph;
use serde::Deserialize;
use std::collections::HashMap;
use std::convert::TryInto;
use std::error::Error;

pub fn init() -> Result<SystemGraphBag, Box<dyn Error>> {
    let flat_systems = parse_systems()?;
    let flat_planet_resources = parse_planet_resources()?;
    let connections = get_connections(&flat_systems);
    let systems = build_systems_from_flat(flat_systems, flat_planet_resources);
    let systems_graph_bag = create_systems_graph(systems, connections)?;
    Ok(systems_graph_bag)
}

fn parse_planet_resources() -> Result<Vec<FlatPlanetResource>, Box<dyn Error>> {
    let mut rdr = ReaderBuilder::new().from_path("planet-resources.csv")?;
    let mut vec = Vec::new();
    for result in rdr.deserialize() {
        vec.push(result?);
    }
    Ok(vec)
}

fn parse_systems() -> Result<Vec<FlatSystem>, Box<dyn Error>> {
    // Build the CSV reader and iterate over each record.
    let mut rdr = ReaderBuilder::new().from_path("systems.csv")?;
    let mut vec = Vec::new();
    for result in rdr.deserialize() {
        vec.push(result?);
    }
    Ok(vec)
}

fn get_connections(systems: &Vec<FlatSystem>) -> HashMap<u32, Vec<u32>> {
    systems
        .iter()
        .map(|s| {
            (
                s.id,
                s.neighbors
                    .split(":")
                    .map(|n| n.parse())
                    .filter_map(|n| n.ok())
                    .collect(),
            )
        })
        .collect()
}

fn build_systems_from_flat(
    systems: Vec<FlatSystem>,
    planet_resources: Vec<FlatPlanetResource>,
) -> Vec<System> {
    // FPRs grouped by system name
    let mut system_groups: HashMap<String, Vec<FlatPlanetResource>> = planet_resources
        .into_iter()
        .fold(HashMap::new(), |mut groups, pr| {
            groups.entry(pr.system.clone()).or_insert(vec![]).push(pr);
            groups
        });
    systems
        .into_iter()
        .filter_map(|system| {
            let prs_in_system = system_groups.remove(&system.name).unwrap_or(vec![]);

            // FPRs grouped by planet id
            let planet_groups: HashMap<u32, Vec<FlatPlanetResource>> = prs_in_system
                .into_iter()
                .fold(HashMap::new(), |mut groups, pr| {
                    groups.entry(pr.id).or_insert(vec![]).push(pr);
                    groups
                });

            let planets = planet_groups
                .into_iter()
                .map(|(_, fprs)| {
                    let id = fprs[0].id;
                    let name = fprs[0].planet_name.clone();
                    let category = fprs[0].planet_category.clone();
                    let resources = fprs
                        .into_iter()
                        .map(|fpr| PlanetResource {
                            resource: fpr.resource.try_into().unwrap(), // Expect correct data in csv
                            richness: fpr.richness.try_into().unwrap(), // Expect correct data in csv
                            output: fpr.output,
                        })
                        .collect();

                    Planet {
                        id,
                        name,
                        category,
                        resources,
                    }
                })
                .collect::<Vec<_>>();

            Some(System {
                id: system.id,
                region: system.region,
                constellation: system.constellation,
                name: system.name,
                security: system.security,
                planets,
            })
        })
        .collect()
}

fn create_systems_graph(
    systems: Vec<System>,
    connections: HashMap<u32, Vec<u32>>,
) -> Result<SystemGraphBag, Box<dyn Error>> {
    let mut graph = Graph::new_undirected();
    let mut id_map = HashMap::new();
    let mut name_lowercase_map = HashMap::new();
    let mut names = Vec::new();

    // Insert all systems as nodes
    for system in systems.into_iter() {
        let id = system.id;
        let name = system.name.clone();
        let index = graph.add_node(system);
        id_map.insert(id, index);
        name_lowercase_map.insert(name.to_lowercase(), index);
        names.push(name);
    }

    // Create edges
    for (original_system_id, connections) in connections.into_iter() {
        let original_node_index = id_map[&original_system_id];
        for neighbor_system_id in connections.into_iter() {
            // Skip if already added
            if neighbor_system_id < original_system_id {
                continue;
            }
            let neighbor_node_index = id_map[&neighbor_system_id];
            graph.add_edge(original_node_index, neighbor_node_index, ());
        }
    }

    Ok(SystemGraphBag {
        graph,
        id_map,
        name_lowercase_map,
        names,
    })
}

#[derive(Debug, Deserialize, Clone)]
struct FlatSystem {
    #[serde(alias = "ID")]
    id: u32,
    // #[serde(alias = "Distance To Jita")]
    // distance_to_jita: u16,
    #[serde(alias = "Region")]
    region: String,
    #[serde(alias = "Constellation")]
    constellation: String,
    #[serde(alias = "Name")]
    name: String,
    #[serde(alias = "Security")]
    security: f64,
    #[serde(alias = "Neighbors")]
    neighbors: String,
    #[serde(alias = "Planets")]
    planets: String,
}

#[derive(Debug, Deserialize)]
struct FlatPlanetResource {
    #[serde(alias = "Planet ID")]
    id: u32,
    #[serde(alias = "Region")]
    region: String,
    #[serde(alias = "Constellation")]
    constellation: String,
    #[serde(alias = "System")]
    system: String,
    #[serde(alias = "Planet Name")]
    planet_name: String,
    #[serde(alias = "Planet Type")]
    planet_category: String,
    #[serde(alias = "Resource")]
    resource: String,
    #[serde(alias = "Richness")]
    richness: String,
    #[serde(alias = "Output")]
    output: f64,
}
