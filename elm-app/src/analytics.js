const trackPage = function(url) {
  ga("set", "page", url);
  ga("send", "pageview");
};

const removeUndefinedValues = object => {
  return Object.keys(object).reduce((acc, key) => {
    if (typeof object[key] !== "undefined") {
      acc[key] = object[key];
    }

    return acc;
  }, {});
};

const trackEvent = function(eventCategory, eventAction, eventLabel) {
  const eventObject = {
    hitType: "event",
    eventCategory,
    eventAction,
    eventLabel
  };

  ga("send", removeUndefinedValues(eventObject));
};

/*
window.ga = function() {
  console.log([...arguments]);
};
*/

const bootstrapGoogleAnalytics = function(propertyId) {
  ga("create", propertyId, "auto");
};

export default class Analytics {
  constructor(propertyId) {
    bootstrapGoogleAnalytics(propertyId);
  }

  navigateToPage(url) {
    trackPage(url);
  }

  updateName(newName) {
    trackEvent("user", "updateName", newName);
  }
}

