module SearchSelectInput exposing (State, input)

import Html exposing (Html, div, text)
import Html.Attributes exposing (class, placeholder, type_, value)
import Html.Events exposing (keyCode, onBlur, onClick, onFocus, onInput, onMouseEnter, preventDefaultOn)
import Json.Decode as Decode


type alias State =
    { value : String
    , matchingOptions : List String
    , highlightedIndex : Maybe Int
    }


type Key
    = Up
    | Down
    | Enter


keyCodeToKey : Int -> Maybe Key
keyCodeToKey keyCode =
    case keyCode of
        38 ->
            Just Up

        40 ->
            Just Down

        13 ->
            Just Enter

        _ ->
            Nothing


onKeyDown : State -> List String -> (State -> msg) -> Html.Attribute msg
onKeyDown state options onChange =
    let
        stateDecoder =
            keyCodeToStatePreventDefault state options onChange
    in
    preventDefaultOn
        "keydown"
        (Decode.map stateDecoder keyCode)


keyCodeToStatePreventDefault : State -> List String -> (State -> msg) -> Int -> ( msg, Bool )
keyCodeToStatePreventDefault state options onChange keyCode =
    case keyCodeToKey keyCode of
        Just key ->
            ( onChange (keyToState state options key), True )

        -- Don't prevent default, unrecognized key!
        Nothing ->
            ( onChange state
            , False
            )


keyToState : State -> List String -> Key -> State
keyToState state options key =
    let
        nextHighlightedIndex =
            case ( key, state.highlightedIndex, List.length state.matchingOptions - 1 ) of
                -- at top item
                ( Up, Just 0, _ ) ->
                    Nothing

                -- none selected
                ( Up, Nothing, _ ) ->
                    Nothing

                -- item selected greater than 0
                ( Up, Just x, _ ) ->
                    Just (x - 1)

                -- item is selected
                ( Down, Just x, y ) ->
                    if x == y then
                        state.highlightedIndex

                    else
                        Just (x + 1)

                -- none selected
                ( Down, Nothing, _ ) ->
                    Just 0

                -- something is selected
                ( Enter, Just _, _ ) ->
                    Nothing

                _ ->
                    state.highlightedIndex

        nextValue =
            case ( key, state.highlightedIndex ) of
                ( Enter, Just i ) ->
                    state.matchingOptions
                        |> List.indexedMap Tuple.pair
                        |> List.filter (\( j, o ) -> i == j)
                        |> List.map (\( j, o ) -> o)
                        |> List.head
                        |> Maybe.withDefault state.value

                _ ->
                    state.value

        nextMatchingOptions =
            case ( key, state.highlightedIndex ) of
                ( Enter, Just i ) ->
                    []

                _ ->
                    state.matchingOptions
    in
    { value = nextValue
    , matchingOptions = nextMatchingOptions
    , highlightedIndex = nextHighlightedIndex
    }


findMatchingOptions : List String -> String -> List String
findMatchingOptions options value =
    let
        matchingOptions =
            List.filter (\o -> String.startsWith (String.toLower value) (String.toLower o)) options

        exactMatch =
            List.any (\o -> String.toLower o == String.toLower value) options
    in
    if exactMatch then
        []

    else
        matchingOptions


newValueToState : State -> List String -> String -> State
newValueToState state options nextValue =
    { value = nextValue
    , matchingOptions = findMatchingOptions options nextValue
    , highlightedIndex = Nothing
    }


input : State -> (State -> msg) -> String -> List String -> Html msg
input state onChange placeholder options =
    let
        inputElement =
            Html.input
                [ type_ "text"
                , Html.Attributes.placeholder placeholder
                , value state.value
                , onKeyDown state options onChange
                , onInput (newValueToState state options >> onChange)
                , onFocus (onChange { state | matchingOptions = findMatchingOptions options state.value, highlightedIndex = Nothing })
                , onBlur (onChange { state | matchingOptions = [], highlightedIndex = Nothing })
                ]
                []

        innerElements =
            case ( state.matchingOptions, String.isEmpty state.value ) of
                ( [], False ) ->
                    [ inputElement ]

                ( matchingOptions, False ) ->
                    if List.length matchingOptions > 20 then
                        [ inputElement ]

                    else
                        [ inputElement, createDropDown state onChange ]

                _ ->
                    [ inputElement ]
    in
    div
        [ class "search-box" ]
        innerElements


createDropDown : State -> (State -> msg) -> Html msg
createDropDown state onChange =
    if List.length state.matchingOptions == 0 then
        div [ class "search-results" ] [ text "No results" ]

    else
        div
            [ class "search-results" ]
            (List.indexedMap (\i v -> createDropDownItem v (Just i == state.highlightedIndex) i state onChange) state.matchingOptions)


createDropDownItem : String -> Bool -> Int -> State -> (State -> msg) -> Html msg
createDropDownItem value isHighlighted index state onChange =
    let
        myClass =
            if isHighlighted then
                "highlighted"

            else
                ""
    in
    div
        [ class myClass
        , onSearchResultClicked (onChange { value = value, highlightedIndex = Nothing, matchingOptions = [] })
        , onMouseEnter (onChange { state | highlightedIndex = Just index })
        ]
        [ text value ]


onSearchResultClicked : msg -> Html.Attribute msg
onSearchResultClicked msg =
    preventDefaultOn "mousedown" (Decode.map alwaysPreventDefault (Decode.succeed msg))


alwaysPreventDefault : msg -> ( msg, Bool )
alwaysPreventDefault msg =
    ( msg, True )
