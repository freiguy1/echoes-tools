module Model exposing (..)

import Browser
import Browser.Navigation as Nav
import Dict exposing (Dict)
import RemoteData exposing (WebData)
import Routing
import SearchSelectInput
import Url exposing (Url)



---- MODEL ----


type alias Model =
    { key : Nav.Key
    , refData : WebData RefData
    , systemInputState : SearchSelectInput.State
    , resourceTypeInput : String
    , resourceRichnessInput : String
    , jumpsInput : Maybe Int
    , searchResults : WebData (List SearchResultPlanet)
    , searchResultsSortBy : List SortByField
    , route : Routing.Route
    }


blankModel : Nav.Key -> Model
blankModel key =
    { key = key
    , refData = RemoteData.NotAsked
    , systemInputState =
        { value = ""
        , matchingOptions = []
        , highlightedIndex = Nothing
        }
    , resourceTypeInput = ""
    , resourceRichnessInput = ""
    , jumpsInput = Nothing
    , searchResults = RemoteData.NotAsked
    , searchResultsSortBy = [ Jumps, ResourceOutput ]
    , route = Routing.Home Nothing
    }


type SortByField
    = Jumps
    | ResourceOutput
    | SystemNameAZ
    | SystemNameZA
    | SystemSecurity
    | PlanetNameAZ
    | PlanetNameZA


sortByFieldIdLookup : Dict Int SortByField
sortByFieldIdLookup =
    Dict.fromList
        [ ( 1, Jumps )
        , ( 2, ResourceOutput )
        , ( 3, SystemNameAZ )
        , ( 4, SystemNameZA )
        , ( 5, SystemSecurity )
        , ( 6, PlanetNameAZ )
        , ( 7, PlanetNameZA )
        ]



-- This indicates if there is no need for another 'then by' sort because it's been sorted by unique values


sortByFieldIsUnique : SortByField -> Bool
sortByFieldIsUnique sortBy =
    case sortBy of
        Jumps ->
            False

        ResourceOutput ->
            False

        SystemNameAZ ->
            False

        SystemNameZA ->
            False

        SystemSecurity ->
            False

        PlanetNameAZ ->
            True

        PlanetNameZA ->
            True


type alias RefData =
    { resources : List String
    , richnesses : List String
    , systems : List String
    }


type alias SearchResultPlanet =
    { jumps : Int
    , systemName : String
    , systemId : Int
    , systemSecurity : Float
    , region : String
    , constellation : String
    , id : Int
    , name : String
    , category : String
    , resources : List PlanetResource
    }


type alias PlanetResource =
    { resource : String
    , richness : String
    , output : Float
    }


type Msg
    = OnUrlChange Url -- don't call directly
    | OnUrlRequest Browser.UrlRequest
    | LoadedRefData (WebData RefData)
    | LoadedSearchResults (WebData (List SearchResultPlanet))
    | SystemInputChanged SearchSelectInput.State
    | ResourceTypeOnInput String
    | ResourceRichnessOnInput String
    | JumpsOnInput String
    | SearchResultsSortByChanged (List SortByField)
