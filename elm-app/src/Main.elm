module Main exposing (..)

import About
import Analytics
import Browser
import Browser.Navigation as Nav
import Html exposing (Html, a, div, header, input, p, span, text)
import Html.Attributes exposing (class, href, placeholder, type_, value)
import Html.Events exposing (onInput)
import Json.Decode as Decode
import Model exposing (Model, Msg(..), SortByField(..))
import RemoteData
import Routing
import SearchResultsSorter
import SearchResultsView
import SearchSelectInput
import Service
import Url exposing (Url)
import Url.Builder
import Url.Parser
import Url.Parser.Query


init : flags -> Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    let
        blankModel =
            Model.blankModel key

        ( model, initCommand ) =
            onUrlChange blankModel url

        searchQuery =
            case model.route of
                Routing.Home q ->
                    q

                _ ->
                    Nothing

        landingOnHomePage =
            case model.route of
                Routing.Home _ ->
                    True

                _ ->
                    False

        systemInputText =
            Maybe.map (\q -> q.system) searchQuery
                |> Maybe.withDefault ""

        resourceTypeInput =
            Maybe.map (\q -> q.resourceType) searchQuery
                |> Maybe.withDefault "Base Metals"

        resourceRichnessInput =
            Maybe.map (\q -> q.richness) searchQuery
                |> Maybe.withDefault "Perfect"

        jumpsInput =
            Maybe.map (\q -> q.jumps) searchQuery
    in
    ( { model
        | systemInputState =
            { value = systemInputText
            , matchingOptions = []
            , highlightedIndex = Nothing
            }
        , resourceTypeInput = resourceTypeInput
        , resourceRichnessInput = resourceRichnessInput
        , jumpsInput = jumpsInput
      }
    , Cmd.batch
        [ initCommand
        , Analytics.trackPageNavigation <| urlToTrackableString url
        ]
    )


urlToTrackableString : Url -> String
urlToTrackableString url =
    let
        query =
            case url.query of
                Just q ->
                    "?" ++ q

                Nothing ->
                    ""
    in
    url.path ++ query



---- UPDATE ----


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LoadedRefData (RemoteData.Success refData) ->
            let
                resources =
                    List.sort refData.resources

                sortedRefData =
                    { refData | resources = resources }

                modelWithRefData =
                    { model | refData = RemoteData.Success sortedRefData }

                validationErrors =
                    getValidationErrors modelWithRefData sortedRefData
            in
            if List.length validationErrors == 0 then
                ( { modelWithRefData | searchResults = RemoteData.Loading }
                , Service.fetchSearchResults
                    model.systemInputState.value
                    model.resourceTypeInput
                    model.resourceRichnessInput
                    (model.jumpsInput |> Maybe.withDefault 1)
                  -- should never be nothing, since validated
                )

            else
                ( modelWithRefData, Cmd.none )

        LoadedRefData refData ->
            ( { model | refData = refData }, Cmd.none )

        LoadedSearchResults (RemoteData.Success searchResults) ->
            let
                sorted =
                    SearchResultsSorter.sort model.searchResultsSortBy model.resourceTypeInput searchResults
            in
            ( { model | searchResults = RemoteData.Success sorted }, Cmd.none )

        LoadedSearchResults results ->
            ( { model | searchResults = results }, Cmd.none )

        SystemInputChanged s ->
            ( { model | systemInputState = s }, Cmd.none )

        ResourceTypeOnInput t ->
            ( { model | resourceTypeInput = t }, Cmd.none )

        ResourceRichnessOnInput r ->
            ( { model | resourceRichnessInput = r }, Cmd.none )

        JumpsOnInput j ->
            ( { model | jumpsInput = String.toInt j }, Cmd.none )

        SearchResultsSortByChanged s ->
            case model.searchResults of
                RemoteData.Success searchResults ->
                    let
                        sorted =
                            SearchResultsSorter.sort s model.resourceTypeInput searchResults
                    in
                    ( { model | searchResultsSortBy = s, searchResults = RemoteData.Success sorted }, Cmd.none )

                _ ->
                    ( { model | searchResultsSortBy = s }, Cmd.none )

        OnUrlRequest urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        OnUrlChange url ->
            let
                ( newModel, cmd ) =
                    onUrlChange model url
            in
            ( newModel, Cmd.batch [ cmd, Analytics.trackPageNavigation <| urlToTrackableString url ] )


onUrlChange : Model -> Url -> ( Model, Cmd Msg )
onUrlChange model url =
    case Url.Parser.parse Routing.parser url of
        Just (Routing.Home maybeQuery) ->
            let
                fetchRefData =
                    case model.refData of
                        RemoteData.Loading ->
                            False

                        RemoteData.Success _ ->
                            False

                        _ ->
                            True

                refData =
                    if fetchRefData then
                        RemoteData.Loading

                    else
                        model.refData
            in
            case maybeQuery of
                Just q ->
                    let
                        fetchSearchResults =
                            case model.route of
                                Routing.Home (Just q2) ->
                                    q.system
                                        /= q2.system
                                        || q.resourceType
                                        /= q2.resourceType
                                        || q.richness
                                        /= q2.richness
                                        || q.jumps
                                        /= q2.jumps

                                _ ->
                                    True

                        cmdMap =
                            [ ( fetchRefData, Service.fetchRefData )

                            -- only search if ref data has been loaded - otherwise we're not sure if query string is legit
                            , ( not fetchRefData && fetchSearchResults, Service.fetchSearchResults q.system q.resourceType q.richness q.jumps )
                            ]

                        cmds =
                            List.filter Tuple.first cmdMap
                                |> List.map Tuple.second

                        searchResults =
                            if fetchSearchResults then
                                RemoteData.Loading

                            else
                                model.searchResults
                    in
                    ( { model | route = Routing.Home (Just q), searchResults = searchResults, refData = refData }, Cmd.batch cmds )

                Nothing ->
                    let
                        cmd =
                            if fetchRefData then
                                Service.fetchRefData

                            else
                                Cmd.none
                    in
                    ( { model | route = Routing.Home Nothing, refData = refData }, cmd )

        Just route ->
            ( { model | route = route }, Cmd.none )

        Nothing ->
            ( { model | route = Routing.NotFound }, Cmd.none )



---- VIEW ----


view : Model -> Browser.Document Msg
view model =
    let
        doc =
            case model.route of
                Routing.Home _ ->
                    homeView model

                Routing.About ->
                    About.view

                Routing.NotFound ->
                    { title = "not found", body = [ text "o/, page not found." ] }

        head =
            header [ class "text-align-right" ]
                [ a [ href "/" ] [ text "home" ]
                , span [] [ text " | " ]
                , a [ href "/about" ] [ text "about" ]
                ]
    in
    { title = doc.title
    , body =
        [ div [ class "wrapper" ] (head :: doc.body) ]
    }


homeView : Model -> Browser.Document Msg
homeView model =
    { title = "EVE: Echoes Tools"
    , body =
        case model.refData of
            RemoteData.Loading ->
                [ p [] [ text "Loading..." ] ]

            RemoteData.Success refData ->
                [ formBody model refData
                , submitButton model refData
                , Html.hr [] []
                , SearchResultsView.view model model.searchResults
                ]

            _ ->
                [ p [] [ text "Problem loading reference data" ] ]
    }


getValidationErrors : Model.Model -> Model.RefData -> List String
getValidationErrors model refData =
    let
        lowerCaseSystems =
            List.map String.toLower refData.systems

        systemIsValid =
            List.member (String.toLower model.systemInputState.value) lowerCaseSystems

        resourceTypeIsValid =
            List.member model.resourceTypeInput refData.resources

        resourceRichnessIsValid =
            List.member model.resourceRichnessInput refData.richnesses

        jumpsIsValid =
            model.jumpsInput |> Maybe.map (\j -> j > 0 && j <= 25) |> Maybe.withDefault False

        errors =
            [ ( systemIsValid, "System name field must match one of the " ++ (lowerCaseSystems |> List.length |> String.fromInt) ++ " known systems" )
            , ( resourceTypeIsValid, "Select a resource type" )
            , ( resourceRichnessIsValid, "Select a resource richness" )
            , ( jumpsIsValid, "Number of jumps must be between 0 and 25" )
            ]

        errorTupleToMaybe =
            \e ->
                if Tuple.first e == False then
                    Just (Tuple.second e)

                else
                    Nothing
    in
    List.filterMap errorTupleToMaybe errors


submitButton : Model.Model -> Model.RefData -> Html Msg
submitButton model refData =
    let
        validationErrors =
            getValidationErrors model refData
    in
    case validationErrors of
        [] ->
            let
                url =
                    Url.Builder.absolute []
                        [ Url.Builder.string "system" model.systemInputState.value
                        , Url.Builder.string "type" model.resourceTypeInput
                        , Url.Builder.string "richness" model.resourceRichnessInput
                        , Url.Builder.int "jumps" (Maybe.withDefault 1 model.jumpsInput)
                        ]

                linkButton =
                    Html.a [ Html.Attributes.href url, Html.Attributes.tabindex -1 ] [ Html.button [] [ text "Search!" ] ]
            in
            if model.resourceTypeInput == "Precious Alloy" && model.resourceRichnessInput == "Perfect" then
                div []
                    [ p [] [ text "A user noticed an issue about Perfect Precious Alloy query never returning any results. Not sure the cause of it, but according to my data, that is correct. So.. just a heads up." ]
                    , linkButton
                    ]

            else
                linkButton

        errors ->
            div []
                [ div []
                    (List.map
                        (\e -> div [] [ text e ])
                        errors
                    )
                , Html.button [ Html.Attributes.disabled True ] [ text "Submit!" ]
                ]


formBody : Model.Model -> Model.RefData -> Html Msg
formBody model refData =
    div [ class "row-or-column" ]
        [ SearchSelectInput.input
            model.systemInputState
            SystemInputChanged
            "System Name"
            refData.systems
        , selectInput model.resourceTypeInput refData.resources ResourceTypeOnInput
        , selectInput model.resourceRichnessInput refData.richnesses ResourceRichnessOnInput
        , input [ type_ "number", placeholder "# jumps (1-25)", value (model.jumpsInput |> Maybe.map String.fromInt |> Maybe.withDefault ""), onInput JumpsOnInput, Html.Attributes.min "1", Html.Attributes.max "25" ] []
        ]


selectInput : String -> List String -> (String -> msg) -> Html msg
selectInput value options toMsg =
    Html.select [ Html.Events.on "change" (Decode.map toMsg Html.Events.targetValue) ]
        (List.map
            (\option -> Html.option [ Html.Attributes.selected (option == value), Html.Attributes.value option ] [ text option ])
            ("" :: options)
        )



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.application
        { view = view
        , init = init
        , update = update
        , subscriptions = always Sub.none
        , onUrlChange = OnUrlChange
        , onUrlRequest = OnUrlRequest
        }
