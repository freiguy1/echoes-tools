module Routing exposing (Route(..), parser)

import Url exposing (Url)
import Url.Parser exposing ((</>), (<?>), Parser, int, map, oneOf, s, string, top)
import Url.Parser.Query as QParser


type Route
    = Home (Maybe SearchQuery)
    | About
    | NotFound


parser : Parser (Route -> a) a
parser =
    oneOf
        [ map Home (top <?> parseSearchQuery)
        , map Home (s "index.html" <?> parseSearchQuery)
        , map About (s "about")
        ]


parseSearchQuery : QParser.Parser (Maybe SearchQuery)
parseSearchQuery =
    let
        maybeParser =
            QParser.map4
                SearchQueryMaybe
                (QParser.string "system")
                (QParser.string "type")
                (QParser.string "richness")
                (QParser.int "jumps")
    in
    QParser.map
        (\m ->
            Maybe.map4 SearchQuery
                m.system
                m.resourceType
                m.richness
                m.jumps
        )
        maybeParser


type alias SearchQuery =
    { system : String
    , resourceType : String
    , richness : String
    , jumps : Int
    }


type alias SearchQueryMaybe =
    { system : Maybe String
    , resourceType : Maybe String
    , richness : Maybe String
    , jumps : Maybe Int
    }
