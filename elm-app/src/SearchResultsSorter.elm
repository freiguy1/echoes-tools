module SearchResultsSorter exposing (sort)

import Model exposing (SearchResultPlanet, SortByField(..))


getResourceOutput : String -> SearchResultPlanet -> Float
getResourceOutput resource planet =
    let
        output =
            planet.resources
                |> List.filter (\r -> r.resource == resource)
                |> List.head
                |> Maybe.map .output
                |> Maybe.withDefault 0.0
    in
    -- negating it so sort is in highest -> lowest
    0 - output


compareByField : SortByField -> String -> SearchResultPlanet -> SearchResultPlanet -> Order
compareByField sortBy resource p1 p2 =
    case sortBy of
        Jumps ->
            compare p1.jumps p2.jumps

        ResourceOutput ->
            compare (getResourceOutput resource p1) (getResourceOutput resource p2)

        SystemNameAZ ->
            compare p1.systemName p2.systemName

        SystemNameZA ->
            compare p2.systemName p1.systemName

        SystemSecurity ->
            compare p2.systemSecurity p1.systemSecurity

        PlanetNameAZ ->
            compare p1.name p2.name

        PlanetNameZA ->
            compare p2.name p1.name


sortListByField : SortByField -> String -> List SearchResultPlanet -> List SearchResultPlanet
sortListByField sortBy resource planets =
    List.sortWith (compareByField sortBy resource) planets


sort : List SortByField -> String -> List SearchResultPlanet -> List SearchResultPlanet
sort sortBys resource planets =
    case sortBys of
        [] ->
            planets

        sortBy :: restSorts ->
            let
                sortedPlanets =
                    sortListByField sortBy resource planets

                groupedPlanets =
                    groupListByField sortBy resource sortedPlanets

                sortedGroupedPlanets =
                    List.map (sort restSorts resource) groupedPlanets
            in
            List.concat sortedGroupedPlanets


groupListByField : SortByField -> String -> List SearchResultPlanet -> List (List SearchResultPlanet)
groupListByField sortBy resource planets =
    List.foldr
        (\planet groups ->
            case groups of
                (p :: ps) :: gs ->
                    if compareByField sortBy resource p planet == EQ then
                        (planet :: (p :: ps)) :: gs

                    else
                        [ planet ] :: (p :: ps) :: gs

                _ ->
                    [ [ planet ] ]
        )
        []
        planets
