module SearchResultsView exposing (view)

import Dict exposing (Dict)
import Html exposing (Html, div, option, select, span, table, td, text, th, tr)
import Html.Attributes exposing (class, selected)
import Html.Events exposing (on, targetValue)
import Json.Decode as Decode
import Model exposing (Model, Msg(..), PlanetResource, SearchResultPlanet, SortByField(..), sortByFieldIdLookup, sortByFieldIsUnique)
import RemoteData
import RichnessSvg exposing (richnessSvg)


view : Model -> RemoteData.WebData (List SearchResultPlanet) -> Html Msg
view model searchResultsWebData =
    case searchResultsWebData of
        RemoteData.Success [] ->
            div [] [ text "No systems found with the parameters above" ]

        RemoteData.Success searchResults ->
            div []
                [ searchResultsSortView model.searchResultsSortBy
                , searchResultsListView model.resourceTypeInput searchResults
                ]

        RemoteData.Failure _ ->
            div [] [ text "there was a problem searching for systems" ]

        _ ->
            div [] []


sortByFieldToString : SortByField -> String
sortByFieldToString sortBy =
    case sortBy of
        Jumps ->
            "Jumps (lowest)"

        ResourceOutput ->
            "Resource output (highest)"

        SystemNameAZ ->
            "System (A > Z)"

        SystemNameZA ->
            "System (Z > A)"

        SystemSecurity ->
            "Security (safest)"

        PlanetNameAZ ->
            "Planet (A > Z)"

        PlanetNameZA ->
            "Planet (Z > A)"


searchResultsSortView : List SortByField -> Html Msg
searchResultsSortView usedSortBys =
    let
        remainingSortBys =
            sortByFieldIdLookup
                |> Dict.filter (\_ v -> not (List.member v usedSortBys))

        reverseUsedSortBys =
            List.reverse usedSortBys

        previousSelects =
            recursiveBuildSearchResultsSortSelect reverseUsedSortBys

        lastSortByIsUnique =
            reverseUsedSortBys
                |> List.head
                |> Maybe.map sortByFieldIsUnique
                |> Maybe.withDefault False

        selects =
            if Dict.size remainingSortBys == 0 || lastSortByIsUnique then
                previousSelects

            else
                List.append
                    previousSelects
                    [ searchResultsSortSelect (decodeStringToListSortByField usedSortBys) -1 remainingSortBys ]
    in
    div [ class "row-or-column" ] selects


recursiveBuildSearchResultsSortSelect : List SortByField -> List (Html Msg)
recursiveBuildSearchResultsSortSelect reverseUsedSortBys =
    case reverseUsedSortBys of
        [] ->
            []

        sortBy :: rest ->
            let
                remainingSortBys =
                    sortByFieldIdLookup
                        |> Dict.filter (\_ v -> not (List.member v rest))

                decoder =
                    decodeStringToListSortByField rest

                id =
                    sortByFieldIdLookup
                        |> Dict.filter (\_ v -> v == sortBy)
                        |> Dict.toList
                        |> List.map (\( k, _ ) -> k)
                        |> List.head
                        |> Maybe.withDefault -1

                nextSelect =
                    searchResultsSortSelect decoder id remainingSortBys

                nextSelects =
                    recursiveBuildSearchResultsSortSelect rest
            in
            List.append nextSelects [ nextSelect ]


searchResultsSortSelect : (String -> Decode.Decoder (List SortByField)) -> Int -> Dict Int SortByField -> Html Msg
searchResultsSortSelect decoder selectedId remainingSortBys =
    let
        isFirst =
            Dict.size remainingSortBys == Dict.size sortByFieldIdLookup

        label =
            if isFirst then
                Html.label [] [ text "Sort by:" ]

            else
                Html.label [] [ text "Then by:" ]

        sortByOptions =
            remainingSortBys
                |> Dict.map (\_ v -> sortByFieldToString v)
                |> Dict.toList

        options =
            ( -1, "" ) :: sortByOptions
    in
    div [ class "sort-by-group" ]
        [ label
        , select
            [ on "change" (Decode.map SearchResultsSortByChanged (Decode.andThen decoder targetValue)) ]
            (List.map
                (\( key, value ) -> option [ selected (selectedId == key), Html.Attributes.value (String.fromInt key) ] [ text value ])
                options
            )
        ]


decodeStringToListSortByField : List SortByField -> String -> Decode.Decoder (List SortByField)
decodeStringToListSortByField previousUsedSortBys stringId =
    case String.toInt stringId of
        Just id ->
            case Dict.get id sortByFieldIdLookup of
                Just sortByField ->
                    Decode.succeed (List.append previousUsedSortBys [ sortByField ])

                Nothing ->
                    -- If an unknown option is selected, clear out dropdown and all next ones
                    Decode.succeed previousUsedSortBys

        Nothing ->
            Decode.fail "Could not parse SortByFieldId from String"


searchResultsListView : String -> List SearchResultPlanet -> Html Msg
searchResultsListView searchedResource searchResults =
    div [] (List.concat (List.map (searchResultView searchedResource) searchResults))


searchResultView : String -> SearchResultPlanet -> List (Html Msg)
searchResultView searchedResource srp =
    [ div [ class "search-result-planet mobile" ]
        [ planetAndSystemInfo srp
        , div [ class "d-flex align-items-center" ]
            [ jumpsInfo srp.jumps
            , div [ class "d-flex flex-wrap" ] (List.map (resourceInfo searchedResource) srp.resources)
            ]
        ]
    , div [ class "search-result-planet desktop d-flex align-items-center" ]
        [ jumpsInfo srp.jumps
        , div []
            [ planetAndSystemInfo srp
            , div [ class "d-flex align-items-center" ]
                [ div [ class "d-flex flex-wrap" ] (List.map (resourceInfo searchedResource) srp.resources)
                ]
            ]
        ]
    ]


securityToString : Float -> String
securityToString security =
    if security < 0.0 then
        "NULL"

    else
        (((security * 10) |> round |> toFloat) / 10)
            |> String.fromFloat


securityToClass : String -> String
securityToClass security =
    case security of
        "NULL" ->
            "color-null"

        "0" ->
            "color-point-0"

        "0.1" ->
            "color-point-1"

        "0.2" ->
            "color-point-2"

        "0.3" ->
            "color-point-3"

        "0.4" ->
            "color-point-4"

        "0.5" ->
            "color-point-5"

        "0.6" ->
            "color-point-6"

        "0.7" ->
            "color-point-7"

        "0.8" ->
            "color-point-8"

        "0.9" ->
            "color-point-9"

        "1" ->
            "color-1"

        _ ->
            "color-1"


planetAndSystemInfo : SearchResultPlanet -> Html Msg
planetAndSystemInfo srp =
    let
        stringSecurity =
            securityToString srp.systemSecurity

        securityClass =
            securityToClass stringSecurity
    in
    div [ class "d-flex justify-content-between" ]
        [ span []
            [ span [ class "bold" ] [ text srp.name ]
            , span [] [ text " (" ]
            , span [ class securityClass ] [ text stringSecurity ]
            , span [] [ text ")" ]
            ]
        , span [] [ text srp.region ]
        ]


jumpsInfo : Int -> Html Msg
jumpsInfo jumps =
    div [ class "jumps" ]
        [ div [] [ jumps |> String.fromInt |> text ]
        , div [] [ text "JUMPS" ]
        ]


resourceInfo : String -> PlanetResource -> Html Msg
resourceInfo searchedResource resource =
    let
        isSearchedResource =
            String.toLower resource.resource == String.toLower searchedResource

        classes =
            if isSearchedResource then
                "resource d-flex align-items-center highlight"

            else
                "resource d-flex align-items-center"
    in
    div [ class classes ]
        [ richnessSvg 12 resource.richness
        , span [] [ text (resource.resource ++ " | " ++ String.fromFloat resource.output) ]
        ]
