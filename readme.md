# How to run app

## Prerequisites:

1. Rust's `cargo` command
2. Node
3. `create-elm-app` installed globally (`npm i -g create-elm-app`)

## Do it

To run both the backend and the frontend requires just a few commands in 2 terminals. The command to run the API: `cargo run`. In the other terminal, `cd elm-app`, then `npm install` then `elm-app start`. You only need to run `npm install` once.

# Production mode

First compile the elm app. In directory elm-app/ run command `elm-app build`. This will create the folder elm-app/build which has all frontend assets. Then back in the root run `cargo run`. The rust app looks for this elm-app/build folder to serve up the elm single page application.


# Docker

There isn't much magic when it comes to docker. `docker build .` will build the image and `docker run -p 3030:3030 <image id>` will run the container. `-p 3030:3030` will expose the containers port 3030 to host port 3030.
