import './main.css';
import { Elm } from './Main.elm';
import * as serviceWorker from './serviceWorker';
import Analytics from "./analytics"

const fetchVariableFromServer = field => {
  const element = document.querySelector(`meta[name=${field}]`);

  if (element) {
    return element.getAttribute("content");
  }
};

// document.addEventListener("DOMContentLoaded", () => {
  const elmApp = Elm.Main.init({
    node: document.getElementById('root')
  });

  const googleAnalyticsId = fetchVariableFromServer("googleAnalyticsId");
  let analytics = null;
  if (googleAnalyticsId) {
    analytics = new Analytics(googleAnalyticsId);
  } else {
    console.log("No valid google analytics id. Not logging.");
  }

  elmApp.ports.trackAnalytics.subscribe(payload => {
    if (analytics) {
      analytics[payload.action](payload.data);
    }
  });
// });

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
