module Service exposing (fetchRefData, fetchSearchResults)

import Http
import Json.Decode as Decode
import Json.Decode.Pipeline exposing (required)
import Model exposing (Msg(..), PlanetResource, RefData, SearchResultPlanet)
import RemoteData
import Url.Builder as UrlBuilder


fetchRefData : Cmd Msg
fetchRefData =
    Http.get
        { url = "/api/refData"
        , expect = Http.expectJson (RemoteData.fromResult >> LoadedRefData) refDataDecoder
        }


refDataDecoder : Decode.Decoder RefData
refDataDecoder =
    Decode.succeed RefData
        |> required "resources" (Decode.list Decode.string)
        |> required "richnesses" (Decode.list Decode.string)
        |> required "systems" (Decode.list Decode.string)


fetchSearchResults : String -> String -> String -> Int -> Cmd Msg
fetchSearchResults system resource richness jumps =
    Http.get
        { url =
            UrlBuilder.absolute
                [ "api"
                , "resources"
                , "search"
                ]
                [ UrlBuilder.string "system" system
                , UrlBuilder.string "type" resource
                , UrlBuilder.string "richness" richness
                , UrlBuilder.int "jumps" jumps
                ]
        , expect = Http.expectJson (RemoteData.fromResult >> LoadedSearchResults) (Decode.list searchResultPlanetDecoder)
        }


searchResultPlanetDecoder : Decode.Decoder SearchResultPlanet
searchResultPlanetDecoder =
    Decode.succeed SearchResultPlanet
        |> required "jumps" Decode.int
        |> required "systemName" Decode.string
        |> required "systemId" Decode.int
        |> required "systemSecurity" Decode.float
        |> required "region" Decode.string
        |> required "constellation" Decode.string
        |> required "id" Decode.int
        |> required "name" Decode.string
        |> required "category" Decode.string
        |> required "resources" (Decode.list planetResourceDecoder)


planetResourceDecoder : Decode.Decoder PlanetResource
planetResourceDecoder =
    Decode.succeed PlanetResource
        |> required "resource" Decode.string
        |> required "richness" Decode.string
        |> required "output" Decode.float
