module RichnessSvg exposing (richnessSvg)

import Html exposing (Html)
import Svg exposing (..)
import Svg.Attributes exposing (..)


colorEmpty =
    "#ededed"


color1 =
    "#a3a3a3"


color2 =
    "#787878"


color3 =
    "#454545"


richnessSvg : Int -> String -> Html msg
richnessSvg dimensions richness =
    let
        inner =
            case richness of
                "Perfect" ->
                    [ rect
                        [ width "3"
                        , height "3"
                        , fill color2
                        ]
                        []
                    , rect
                        [ width "1"
                        , height "1"
                        , x "1"
                        , y "1"
                        , fill color3
                        ]
                        []
                    ]

                "Rich" ->
                    [ rect
                        [ width "3"
                        , height "3"
                        , fill colorEmpty
                        ]
                        []
                    , rect
                        [ width "1"
                        , height "1"
                        , x "2"
                        , y "2"
                        , fill color1
                        ]
                        []
                    , rect
                        [ width "1"
                        , height "1"
                        , x "1"
                        , y "2"
                        , fill color2
                        ]
                        []
                    , rect
                        [ width "1"
                        , height "1"
                        , x "2"
                        , y "1"
                        , fill color2
                        ]
                        []
                    , rect
                        [ width "1"
                        , height "1"
                        , x "0"
                        , y "2"
                        , fill color3
                        ]
                        []
                    , rect
                        [ width "1"
                        , height "1"
                        , x "1"
                        , y "1"
                        , fill color3
                        ]
                        []
                    , rect
                        [ width "1"
                        , height "1"
                        , x "2"
                        , y "0"
                        , fill color3
                        ]
                        []
                    ]

                "Medium" ->
                    [ rect
                        [ width "3"
                        , height "3"
                        , fill colorEmpty
                        ]
                        []
                    , rect
                        [ width "1"
                        , height "1"
                        , x "2"
                        , y "2"
                        , fill color1
                        ]
                        []
                    , rect
                        [ width "1"
                        , height "1"
                        , x "1"
                        , y "2"
                        , fill color2
                        ]
                        []
                    , rect
                        [ width "1"
                        , height "1"
                        , x "2"
                        , y "1"
                        , fill color2
                        ]
                        []
                    ]

                "Poor" ->
                    [ rect
                        [ width "3"
                        , height "3"
                        , fill colorEmpty
                        ]
                        []
                    , rect
                        [ width "1"
                        , height "1"
                        , x "2"
                        , y "2"
                        , fill color1
                        ]
                        []
                    ]

                _ ->
                    [ rect
                        [ width "3"
                        , height "3"
                        , fill colorEmpty
                        ]
                        []
                    ]
    in
    svg
        [ width (String.fromInt dimensions)
        , height (String.fromInt dimensions)
        , viewBox "0 0 3 3"
        ]
        inner



{-
   <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" version="1.1" viewBox="0 0 3 3">
     <rect width="3" height="3" fill="#454545" />
       <rect width="1" height="1" x="1" y="1" fill="#1c1c1c"/>
       </svg>
-}
