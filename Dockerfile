######### Rust build container ############
FROM rust:1.46.0-alpine as rust-build-env

WORKDIR /program

COPY Cargo.lock .
COPY Cargo.toml .
COPY src src

RUN apk add --no-cache musl-dev

RUN cargo build --release


######## Elm build container ##########
FROM node:14.10 as elm-build-env

# --unsafe because I was getting permission error
RUN npm i -g --unsafe create-elm-app

WORKDIR /program

COPY elm-app .

RUN npm install

RUN elm-app build


######## Runtime container ###########
FROM alpine:3.12.0

WORKDIR /program

COPY planet-resources.csv .
COPY systems.csv .

COPY --from=rust-build-env /program/target/release/echoes-tools .
COPY --from=elm-build-env /program/build elm-app/build/

CMD ./echoes-tools
