module About exposing (view)

import Browser
import Html exposing (a, h3, p, text)
import Html.Attributes exposing (class, href)
import Model exposing (Msg)


view : Browser.Document Msg
view =
    { title = "about"
    , body =
        [ h3 [] [ text "this website" ]
        , p [] [ text "This site is a hobby project that I work on in my spare time. It currently has one function: planetary resource search, but I may add more tools eventually." ]
        , h3 [] [ text "technical info" ]
        , p []
            [ text "Here is a rundown of the technologies used in this app. The front end is build in "
            , a [ href "https://elm-lang.org/" ] [ text "elm" ]
            , text ", and the back end is built in "
            , a [ href "https://rust-lang.org/" ] [ text "rust" ]
            , text ". The system/resource data is stored in the rust app memory in a graph data structure using "
            , a [ href "https://crates.io/crates/petgraph" ] [ text "petgraph" ]
            , text ". This allows for very fast lookups and why you hopefully have seen nary a \"loading\" message."
            , text " The application can be run as a docker container and that's how it is deployed to Heroku. The base image for the docker container is Alpine Linux allowing the runtime docker image to only be 25MB."
            , text " You can check out the source code on "
            , a [ href "https://gitlab.com/freiguy1/echoes-tools" ] [ text "gitlab" ]
            , text "."
            ]
        , h3 [] [ text "discussion" ]
        , p []
            [ text "Please "
            , a [ href "https://www.reddit.com/r/echoes/comments/iuya8u/i_made_an_ugly_web_app_which_searches_for/" ] [ text "join the discussion on reddit" ]
            , text " or file an issue on "
            , a [ href "https://gitlab.com/freiguy1/echoes-tools/-/issues" ] [ text "gitlab" ]
            , text " to communicate with me! Anything from constructive criticism to feature requests to compliments is welcome."
            ]
        ]
    }
