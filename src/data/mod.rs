use petgraph::prelude::*;
use serde::{Serialize, Serializer};
use std::collections::{HashMap, HashSet};
use std::convert::TryFrom;

mod init;

pub use init::init;

fn _shortest_path(
    gb: &SystemGraphBag,
    node_index_1: NodeIndex,
    node_index_2: NodeIndex,
) -> Option<u32> {
    let shortest_path =
        petgraph::algo::dijkstra(&gb.graph, node_index_1, Some(node_index_2), |_| 1);
    shortest_path.get(&node_index_2).map(|sp| *sp)
}

fn layers_from_system(
    gb: &SystemGraphBag,
    system_node_index: NodeIndex,
    layer_count: u8,
) -> Vec<Vec<NodeIndex>> {
    fn peel_next_layer(
        gb: &SystemGraphBag,
        current_layer_indices: HashSet<NodeIndex>,
        mut visited_indices: HashSet<NodeIndex>,
        layers_left: u8,
    ) -> Vec<Vec<NodeIndex>> {
        if layers_left == 0 {
            return vec![];
        }

        let mut next_layer: HashSet<NodeIndex> = HashSet::new();
        for &system_index in current_layer_indices.iter() {
            for next_layer_system_index in gb.graph.neighbors(system_index) {
                if visited_indices.contains(&next_layer_system_index)
                    || next_layer.contains(&next_layer_system_index)
                {
                    continue;
                }
                visited_indices.insert(next_layer_system_index);
                next_layer.insert(next_layer_system_index);
            }
        }

        let next_layer_vec: Vec<NodeIndex> = next_layer.iter().cloned().collect();
        let mut recurse_result = peel_next_layer(gb, next_layer, visited_indices, layers_left - 1);
        let mut result = vec![next_layer_vec];
        result.append(&mut recurse_result);
        result
    }

    let mut result = vec![vec![system_node_index]];
    let mut recurse_result = peel_next_layer(
        gb,
        vec![system_node_index].into_iter().collect(),
        vec![system_node_index].into_iter().collect(),
        layer_count,
    );
    result.append(&mut recurse_result);
    result
}

pub fn find_resources<'a>(
    sgb: &'a SystemGraphBag,
    node_index: NodeIndex,
    resource: ResourceType,
    richness: ResourceRichness,
    distance: u8,
) -> Vec<PlanetResourceSearchResult> {
    let layers = layers_from_system(&sgb, node_index, distance);
    layers
        .iter()
        .enumerate()
        .map(|(index, layer)| {
            layer
                .iter()
                .map(|ni| sgb.graph.node_weight(*ni).unwrap())
                .flat_map(|s| {
                    s.planets
                        .iter()
                        .filter(|p| {
                            p.resources
                                .iter()
                                .any(|r| r.resource == resource && r.richness >= richness)
                        })
                        .map(move |p| PlanetResourceSearchResult::new(p, s, index as u8))
                })
                .collect::<Vec<PlanetResourceSearchResult>>()
        })
        .flat_map(|planets| planets)
        .collect()
}

#[derive(Debug, Serialize)]
pub struct PlanetResourceSearchResult {
    pub jumps: u8,
    #[serde(rename = "systemName")]
    pub system_name: String,
    #[serde(rename = "systemId")]
    pub system_id: u32,
    #[serde(rename = "systemSecurity")]
    pub system_security: f64,
    pub region: String,
    pub constellation: String,
    pub id: u32,
    pub name: String,
    pub category: String,
    pub resources: Vec<PlanetResource>,
}

impl PlanetResourceSearchResult {
    fn new(p: &Planet, s: &System, jumps: u8) -> Self {
        PlanetResourceSearchResult {
            jumps: jumps,
            system_name: s.name.clone(),
            system_id: s.id,
            system_security: s.security,
            region: s.region.clone(),
            constellation: s.constellation.clone(),
            id: p.id,
            name: p.name.clone(),
            category: p.category.clone(),
            resources: p.resources.clone(),
        }
    }
}

pub struct SystemGraphBag {
    pub graph: Graph<System, (), petgraph::Undirected>,
    pub id_map: HashMap<u32, NodeIndex>,
    pub name_lowercase_map: HashMap<String, NodeIndex>,
    pub names: Vec<String>,
}

pub struct System {
    pub id: u32,
    pub region: String,
    pub constellation: String,
    pub name: String,
    pub security: f64,
    pub planets: Vec<Planet>,
}

pub struct Planet {
    pub id: u32,
    pub name: String,
    pub category: String,
    pub resources: Vec<PlanetResource>,
}

#[derive(Clone, Debug, Serialize)]
pub struct PlanetResource {
    pub resource: ResourceType,
    pub richness: ResourceRichness,
    pub output: f64,
}

const POLYARAMIDS: &'static str = "Polyaramids";
const COOLANT: &'static str = "Coolant";
const CRYSTAL_COMPOUND: &'static str = "Crystal Compound";
const OXYGEN_ISOTOPES: &'static str = "Oxygen Isotopes";
const IONIC_SOLUTIONS: &'static str = "Ionic Solutions";
const FIBER_COMPOSITE: &'static str = "Fiber Composite";
const NOBLE_METALS: &'static str = "Noble Metals";
const INDUSTRIAL_FIBERS: &'static str = "Industrial Fibers";
const OPULENT_COMPOUND: &'static str = "Opulent Compound";
const REACTIVE_METALS: &'static str = "Reactive Metals";
const HEAVY_WATER: &'static str = "Heavy Water";
const LUCENT_COMPOUND: &'static str = "Lucent Compound";
const NOBLE_GAS: &'static str = "Noble Gas";
const PRECIOUS_ALLOY: &'static str = "Precious Alloy";
const GLEAMING_ALLOY: &'static str = "Gleaming Alloy";
const GLOSSY_COMPOUND: &'static str = "Glossy Compound";
const SHEEN_COMPOUND: &'static str = "Sheen Compound";
const SILICATE_GLASS: &'static str = "Silicate Glass";
const CONDENSED_ALLOY: &'static str = "Condensed Alloy";
const REACTIVE_GAS: &'static str = "Reactive Gas";
const DARK_COMPOUND: &'static str = "Dark Compound";
const CONSTRUCTION_BLOCKS: &'static str = "Construction Blocks";
const LUSTERING_ALLOY: &'static str = "Lustering Alloy";
const BASE_METALS: &'static str = "Base Metals";
const HEAVY_METALS: &'static str = "Heavy Metals";
const NANITES: &'static str = "Nanites";
const PLASMOIDS: &'static str = "Plasmoids";
const SMARTFAB_UNITS: &'static str = "Smartfab Units";
const TOXIC_METALS: &'static str = "Toxic Metals";
const SUSPENDED_PLASMA: &'static str = "Suspended Plasma";
const LIQUID_OZONE: &'static str = "Liquid Ozone";
const MOTLEY_COMPOUND: &'static str = "Motley Compound";
const SUPERTENSILE_PLASTICS: &'static str = "Supertensile Plastics";
const CONDENSATES: &'static str = "Condensates";

#[derive(PartialEq, Clone, Debug)]
pub enum ResourceType {
    Polyaramids,
    Coolant,
    CrystalCompound,
    OxygenIsotopes,
    IonicSolutions,
    FiberComposite,
    NobleMetals,
    IndustrialFibers,
    OpulentCompound,
    ReactiveMetals,
    HeavyWater,
    LucentCompound,
    NobleGas,
    PreciousAlloy,
    GleamingAlloy,
    GlossyCompound,
    SheenCompound,
    SilicateGlass,
    CondensedAlloy,
    ReactiveGas,
    DarkCompound,
    ConstructionBlocks,
    LusteringAlloy,
    BaseMetals,
    HeavyMetals,
    Nanites,
    Plasmoids,
    SmartfabUnits,
    ToxicMetals,
    SuspendedPlasma,
    LiquidOzone,
    MotleyCompound,
    SupertensilePlastics,
    Condensates,
}

impl ResourceType {
    pub fn all() -> Vec<Self> {
        vec![
            Self::Polyaramids,
            Self::Coolant,
            Self::CrystalCompound,
            Self::OxygenIsotopes,
            Self::IonicSolutions,
            Self::FiberComposite,
            Self::NobleMetals,
            Self::IndustrialFibers,
            Self::OpulentCompound,
            Self::ReactiveMetals,
            Self::HeavyWater,
            Self::LucentCompound,
            Self::NobleGas,
            Self::PreciousAlloy,
            Self::GleamingAlloy,
            Self::GlossyCompound,
            Self::SheenCompound,
            Self::SilicateGlass,
            Self::CondensedAlloy,
            Self::ReactiveGas,
            Self::DarkCompound,
            Self::ConstructionBlocks,
            Self::LusteringAlloy,
            Self::BaseMetals,
            Self::HeavyMetals,
            Self::Nanites,
            Self::Plasmoids,
            Self::SmartfabUnits,
            Self::ToxicMetals,
            Self::SuspendedPlasma,
            Self::LiquidOzone,
            Self::MotleyCompound,
            Self::SupertensilePlastics,
            Self::Condensates,
        ]
    }
}

impl ToString for ResourceType {
    fn to_string(&self) -> String {
        match self {
            Self::Polyaramids => POLYARAMIDS.to_owned(),
            Self::Coolant => COOLANT.to_owned(),
            Self::CrystalCompound => CRYSTAL_COMPOUND.to_owned(),
            Self::OxygenIsotopes => OXYGEN_ISOTOPES.to_owned(),
            Self::IonicSolutions => IONIC_SOLUTIONS.to_owned(),
            Self::FiberComposite => FIBER_COMPOSITE.to_owned(),
            Self::NobleMetals => NOBLE_METALS.to_owned(),
            Self::IndustrialFibers => INDUSTRIAL_FIBERS.to_owned(),
            Self::OpulentCompound => OPULENT_COMPOUND.to_owned(),
            Self::ReactiveMetals => REACTIVE_METALS.to_owned(),
            Self::HeavyWater => HEAVY_WATER.to_owned(),
            Self::LucentCompound => LUCENT_COMPOUND.to_owned(),
            Self::NobleGas => NOBLE_GAS.to_owned(),
            Self::PreciousAlloy => PRECIOUS_ALLOY.to_owned(),
            Self::GleamingAlloy => GLEAMING_ALLOY.to_owned(),
            Self::GlossyCompound => GLOSSY_COMPOUND.to_owned(),
            Self::SheenCompound => SHEEN_COMPOUND.to_owned(),
            Self::SilicateGlass => SILICATE_GLASS.to_owned(),
            Self::CondensedAlloy => CONDENSED_ALLOY.to_owned(),
            Self::ReactiveGas => REACTIVE_GAS.to_owned(),
            Self::DarkCompound => DARK_COMPOUND.to_owned(),
            Self::ConstructionBlocks => CONSTRUCTION_BLOCKS.to_owned(),
            Self::LusteringAlloy => LUSTERING_ALLOY.to_owned(),
            Self::BaseMetals => BASE_METALS.to_owned(),
            Self::HeavyMetals => HEAVY_METALS.to_owned(),
            Self::Nanites => NANITES.to_owned(),
            Self::Plasmoids => PLASMOIDS.to_owned(),
            Self::SmartfabUnits => SMARTFAB_UNITS.to_owned(),
            Self::ToxicMetals => TOXIC_METALS.to_owned(),
            Self::SuspendedPlasma => SUSPENDED_PLASMA.to_owned(),
            Self::LiquidOzone => LIQUID_OZONE.to_owned(),
            Self::MotleyCompound => MOTLEY_COMPOUND.to_owned(),
            Self::SupertensilePlastics => SUPERTENSILE_PLASTICS.to_owned(),
            Self::Condensates => CONDENSATES.to_owned(),
        }
    }
}

impl TryFrom<String> for ResourceType {
    type Error = String;

    fn try_from(rt: String) -> Result<Self, Self::Error> {
        match rt.as_str() {
            POLYARAMIDS => Ok(ResourceType::Polyaramids),
            COOLANT => Ok(ResourceType::Coolant),
            CRYSTAL_COMPOUND => Ok(ResourceType::CrystalCompound),
            OXYGEN_ISOTOPES => Ok(ResourceType::OxygenIsotopes),
            IONIC_SOLUTIONS => Ok(ResourceType::IonicSolutions),
            FIBER_COMPOSITE => Ok(ResourceType::FiberComposite),
            NOBLE_METALS => Ok(ResourceType::NobleMetals),
            INDUSTRIAL_FIBERS => Ok(ResourceType::IndustrialFibers),
            OPULENT_COMPOUND => Ok(ResourceType::OpulentCompound),
            REACTIVE_METALS => Ok(ResourceType::ReactiveMetals),
            HEAVY_WATER => Ok(ResourceType::HeavyWater),
            LUCENT_COMPOUND => Ok(ResourceType::LucentCompound),
            NOBLE_GAS => Ok(ResourceType::NobleGas),
            PRECIOUS_ALLOY => Ok(ResourceType::PreciousAlloy),
            GLEAMING_ALLOY => Ok(ResourceType::GleamingAlloy),
            GLOSSY_COMPOUND => Ok(ResourceType::GlossyCompound),
            SHEEN_COMPOUND => Ok(ResourceType::SheenCompound),
            SILICATE_GLASS => Ok(ResourceType::SilicateGlass),
            CONDENSED_ALLOY => Ok(ResourceType::CondensedAlloy),
            REACTIVE_GAS => Ok(ResourceType::ReactiveGas),
            DARK_COMPOUND => Ok(ResourceType::DarkCompound),
            CONSTRUCTION_BLOCKS => Ok(ResourceType::ConstructionBlocks),
            LUSTERING_ALLOY => Ok(ResourceType::LusteringAlloy),
            BASE_METALS => Ok(ResourceType::BaseMetals),
            HEAVY_METALS => Ok(ResourceType::HeavyMetals),
            NANITES => Ok(ResourceType::Nanites),
            PLASMOIDS => Ok(ResourceType::Plasmoids),
            SMARTFAB_UNITS => Ok(ResourceType::SmartfabUnits),
            TOXIC_METALS => Ok(ResourceType::ToxicMetals),
            SUSPENDED_PLASMA => Ok(ResourceType::SuspendedPlasma),
            LIQUID_OZONE => Ok(ResourceType::LiquidOzone),
            MOTLEY_COMPOUND => Ok(ResourceType::MotleyCompound),
            SUPERTENSILE_PLASTICS => Ok(ResourceType::SupertensilePlastics),
            CONDENSATES => Ok(ResourceType::Condensates),
            s => Err(format!("Unknown resource type of {}", s)),
        }
    }
}

impl Serialize for ResourceType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

const POOR: &'static str = "Poor";
const MEDIUM: &'static str = "Medium";
const RICH: &'static str = "Rich";
const PERFECT: &'static str = "Perfect";

#[derive(PartialEq, PartialOrd, Clone, Debug, Serialize)]
pub enum ResourceRichness {
    Poor,
    Medium,
    Rich,
    Perfect,
}

impl ResourceRichness {
    pub fn all() -> Vec<Self> {
        vec![Self::Poor, Self::Medium, Self::Rich, Self::Perfect]
    }
}

impl TryFrom<String> for ResourceRichness {
    type Error = String;

    fn try_from(rt: String) -> Result<Self, Self::Error> {
        match rt.as_str() {
            POOR => Ok(ResourceRichness::Poor),
            MEDIUM => Ok(ResourceRichness::Medium),
            RICH => Ok(ResourceRichness::Rich),
            PERFECT => Ok(ResourceRichness::Perfect),
            s => Err(format!("Unknown resource richness of {}", s)),
        }
    }
}

impl ToString for ResourceRichness {
    fn to_string(&self) -> String {
        match self {
            Self::Poor => POOR.to_owned(),
            Self::Medium => MEDIUM.to_owned(),
            Self::Rich => RICH.to_owned(),
            Self::Perfect => PERFECT.to_owned(),
        }
    }
}
