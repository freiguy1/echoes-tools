use serde::{Deserialize, Serialize};
use std::convert::Infallible;
use std::process;
use std::sync::Arc;
use warp::http::StatusCode;
use warp::{Filter, Rejection, Reply};

mod data;

const WEB_APP_DIR: &'static str = "elm-app/build";

#[tokio::main]
async fn main() {
    print!("Parsing data... ");

    let graph_bag = match data::init() {
        Ok(gb) => gb,
        Err(e) => {
            eprintln!("Initialize data failed! {}", e);
            process::exit(1);
        }
    };

    print!("Success!\nStarting server... ");

    insert_metadata_into_html();
    let ref_data = Arc::new(RefData::new(&graph_bag));
    let graph_bag = Arc::new(graph_bag);

    // Api routes first
    let routes = warp::path("api")
        .and(
            warp::path!("resources" / "search")
                .and(warp::query::<ResourceSearchQueryFormat>())
                .and(warp::any().map(move || graph_bag.clone()))
                .and_then(resource_search_handler)
                .or(warp::path!("refData")
                    .and(warp::any().map(move || Arc::clone(&ref_data)))
                    .map(|ref_data: Arc<RefData>| warp::reply::json(&*ref_data)))
                .recover(recover_api),
        )
        // Web app proxy
        .or(warp::fs::dir(WEB_APP_DIR).or(warp::fs::file(format!("{}/index.html", WEB_APP_DIR))));

    let port = std::env::var("PORT")
        .ok()
        .and_then(|p| p.parse().ok())
        .unwrap_or(3030);

    println!("Success on port {}!", port);
    warp::serve(routes).run(([0, 0, 0, 0], port)).await;
}

fn insert_metadata_into_html() {
    let path = format!("{}/index.html", WEB_APP_DIR);
    let mut file_contents = match std::fs::read_to_string(&path) {
        Ok(s) => s,
        Err(_) => {
            println!("Front end assets weren't built with elm-app build command. This is fine for developing.");
            return;
        }
    };

    let index_of_start_head = match file_contents.find("<head>") {
        Some(i) => i + 6,
        None => {
            eprintln!("Index.html incorrectly built. Contains no opening <head> tag");
            return;
        }
    };

    let hidden_fields = vec![(
        "googleAnalyticsId",
        std::env::var("GOOGLE_ANALYTICS_ID").unwrap_or("unknown".to_string()),
    )];
    let formatted: String = hidden_fields
        .into_iter()
        .map(|(k, v)| format!("<meta name=\"{}\" content=\"{}\">", k, v))
        .flat_map(|i| i.chars().collect::<Vec<_>>())
        .collect();
    file_contents.insert_str(index_of_start_head, &formatted);

    match std::fs::write(&path, file_contents) {
        Ok(()) => {}
        Err(_) => eprintln!("Could not write back to index.html after inserting metadata"),
    }
}

async fn recover_api(_: Rejection) -> Result<impl Reply, Infallible> {
    Ok(StatusCode::NOT_FOUND)
}

#[derive(Serialize)]
struct RefData {
    resources: Vec<String>,
    richnesses: Vec<String>,
    systems: Vec<String>,
}

impl RefData {
    fn new(graph_bag: &data::SystemGraphBag) -> Self {
        RefData {
            resources: data::ResourceType::all()
                .into_iter()
                .map(|r| r.to_string())
                .collect(),
            richnesses: data::ResourceRichness::all()
                .into_iter()
                .map(|r| r.to_string())
                .collect(),
            systems: graph_bag.names.iter().cloned().collect(), //name_map.keys().map(|s| s.clone()).collect(),
        }
    }
}

async fn resource_search_handler(
    opts: ResourceSearchQueryFormat,
    graph_bag: Arc<data::SystemGraphBag>,
) -> Result<impl warp::Reply, Infallible> {
    use std::convert::TryInto;

    let resource_type: data::ResourceType = match opts.resource_type.clone().try_into() {
        Ok(rt) => rt,
        Err(_) => {
            let json = warp::reply::json(&format!("{} unknown resource type", opts.resource_type));
            return Ok(warp::reply::with_status(json, StatusCode::BAD_REQUEST));
        }
    };

    let resource_richness: data::ResourceRichness = match opts.resource_richness.clone().try_into()
    {
        Ok(rr) => rr,
        Err(_) => {
            let json = warp::reply::json(&format!(
                "{} unknown resource richness",
                opts.resource_richness
            ));
            return Ok(warp::reply::with_status(json, StatusCode::BAD_REQUEST));
        }
    };

    let graph_bag = &*graph_bag;
    let system_node_index = match graph_bag
        .name_lowercase_map
        .get(&opts.system.to_lowercase())
    {
        Some(sni) => sni,
        None => {
            let json = warp::reply::json(&format!("{} unknown system", opts.system));
            return Ok(warp::reply::with_status(json, StatusCode::BAD_REQUEST));
        }
    };

    if opts.jumps > 25 {
        let json = warp::reply::json(&format!("# jumps must be < 25. instead was {}", opts.jumps));
        return Ok(warp::reply::with_status(json, StatusCode::BAD_REQUEST));
    }

    let planets = data::find_resources(
        &graph_bag,
        *system_node_index,
        resource_type.clone(),
        resource_richness.clone(),
        opts.jumps,
    );

    let json = warp::reply::json(&planets);
    Ok(warp::reply::with_status(json, StatusCode::OK))
}

#[derive(Deserialize)]
struct ResourceSearchQueryFormat {
    system: String,
    #[serde(alias = "type")]
    resource_type: String,
    #[serde(alias = "richness")]
    resource_richness: String,
    jumps: u8,
}
